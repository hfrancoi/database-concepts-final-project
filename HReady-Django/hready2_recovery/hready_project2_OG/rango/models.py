# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.contrib.auth.models import User

class Admissions(models.Model):
    row_id = models.PositiveSmallIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID', unique=True)  # Field name made lowercase.
    admittime = models.DateTimeField(db_column='ADMITTIME')  # Field name made lowercase.
    dischtime = models.DateTimeField(db_column='DISCHTIME')  # Field name made lowercase.
    deathtime = models.DateTimeField(db_column='DEATHTIME', blank=True, null=True)  # Field name made lowercase.
    admission_type = models.CharField(db_column='ADMISSION_TYPE', max_length=255)  # Field name made lowercase.
    admission_location = models.CharField(db_column='ADMISSION_LOCATION', max_length=255)  # Field name made lowercase.
    discharge_location = models.CharField(db_column='DISCHARGE_LOCATION', max_length=255)  # Field name made lowercase.
    insurance = models.CharField(db_column='INSURANCE', max_length=255)  # Field name made lowercase.
    language = models.CharField(db_column='LANGUAGE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    religion = models.CharField(db_column='RELIGION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    marital_status = models.CharField(db_column='MARITAL_STATUS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ethnicity = models.CharField(db_column='ETHNICITY', max_length=255)  # Field name made lowercase.
    edregtime = models.DateTimeField(db_column='EDREGTIME', blank=True, null=True)  # Field name made lowercase.
    edouttime = models.DateTimeField(db_column='EDOUTTIME', blank=True, null=True)  # Field name made lowercase.
    diagnosis = models.TextField(db_column='DIAGNOSIS', blank=True, null=True)  # Field name made lowercase.
    hospital_expire_flag = models.PositiveIntegerField(db_column='HOSPITAL_EXPIRE_FLAG')  # Field name made lowercase.
    has_chartevents_data = models.PositiveIntegerField(db_column='HAS_CHARTEVENTS_DATA')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ADMISSIONS'


class Callout(models.Model):
    row_id = models.PositiveSmallIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID')  # Field name made lowercase.
    submit_wardid = models.PositiveIntegerField(db_column='SUBMIT_WARDID', blank=True, null=True)  # Field name made lowercase.
    submit_careunit = models.CharField(db_column='SUBMIT_CAREUNIT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    curr_wardid = models.PositiveIntegerField(db_column='CURR_WARDID', blank=True, null=True)  # Field name made lowercase.
    curr_careunit = models.CharField(db_column='CURR_CAREUNIT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    callout_wardid = models.PositiveIntegerField(db_column='CALLOUT_WARDID')  # Field name made lowercase.
    callout_service = models.CharField(db_column='CALLOUT_SERVICE', max_length=255)  # Field name made lowercase.
    request_tele = models.PositiveIntegerField(db_column='REQUEST_TELE')  # Field name made lowercase.
    request_resp = models.PositiveIntegerField(db_column='REQUEST_RESP')  # Field name made lowercase.
    request_cdiff = models.PositiveIntegerField(db_column='REQUEST_CDIFF')  # Field name made lowercase.
    request_mrsa = models.PositiveIntegerField(db_column='REQUEST_MRSA')  # Field name made lowercase.
    request_vre = models.PositiveIntegerField(db_column='REQUEST_VRE')  # Field name made lowercase.
    callout_status = models.CharField(db_column='CALLOUT_STATUS', max_length=255)  # Field name made lowercase.
    callout_outcome = models.CharField(db_column='CALLOUT_OUTCOME', max_length=255)  # Field name made lowercase.
    discharge_wardid = models.PositiveIntegerField(db_column='DISCHARGE_WARDID', blank=True, null=True)  # Field name made lowercase.
    acknowledge_status = models.CharField(db_column='ACKNOWLEDGE_STATUS', max_length=255)  # Field name made lowercase.
    createtime = models.DateTimeField(db_column='CREATETIME')  # Field name made lowercase.
    updatetime = models.DateTimeField(db_column='UPDATETIME')  # Field name made lowercase.
    acknowledgetime = models.DateTimeField(db_column='ACKNOWLEDGETIME', blank=True, null=True)  # Field name made lowercase.
    outcometime = models.DateTimeField(db_column='OUTCOMETIME')  # Field name made lowercase.
    firstreservationtime = models.DateTimeField(db_column='FIRSTRESERVATIONTIME', blank=True, null=True)  # Field name made lowercase.
    currentreservationtime = models.DateTimeField(db_column='CURRENTRESERVATIONTIME', unique=True, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CALLOUT'


class Caregivers(models.Model):
    row_id = models.PositiveSmallIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    cgid = models.PositiveSmallIntegerField(db_column='CGID', unique=True)  # Field name made lowercase.
    label = models.CharField(db_column='LABEL', max_length=255, blank=True, null=True)  # Field name made lowercase.
    description = models.CharField(db_column='DESCRIPTION', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CAREGIVERS'


class Cases(models.Model):
    case_id = models.AutoField(db_column='CASE_ID', primary_key=True)  # Field name made lowercase.
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    cpt_number = models.IntegerField(db_column='CPT_NUMBER', blank=True, null=True)  # Field name made lowercase.
    cgid = models.IntegerField(db_column='CGID', blank=True, null=True)  # Field name made lowercase.
    diagnosis = models.CharField(db_column='DIAGNOSIS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    starttime = models.DateTimeField(db_column='STARTTIME')  # Field name made lowercase.
    endtime = models.DateTimeField(db_column='ENDTIME')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CASES'


class Chartevents(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID', blank=True, null=True)  # Field name made lowercase.
    icustay_id = models.PositiveIntegerField(db_column='ICUSTAY_ID', blank=True, null=True)  # Field name made lowercase.
    itemid = models.PositiveIntegerField(db_column='ITEMID')  # Field name made lowercase.
    charttime = models.DateTimeField(db_column='CHARTTIME')  # Field name made lowercase.
    storetime = models.DateTimeField(db_column='STORETIME', blank=True, null=True)  # Field name made lowercase.
    cgid = models.PositiveSmallIntegerField(db_column='CGID', blank=True, null=True)  # Field name made lowercase.
    value = models.TextField(db_column='VALUE', blank=True, null=True)  # Field name made lowercase.
    valuenum = models.FloatField(db_column='VALUENUM', blank=True, null=True)  # Field name made lowercase.
    valueuom = models.CharField(db_column='VALUEUOM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    warning = models.PositiveIntegerField(db_column='WARNING', blank=True, null=True)  # Field name made lowercase.
    error = models.PositiveIntegerField(db_column='ERROR', blank=True, null=True)  # Field name made lowercase.
    resultstatus = models.CharField(db_column='RESULTSTATUS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    stopped = models.CharField(db_column='STOPPED', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CHARTEVENTS'


class Cptevents(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID')  # Field name made lowercase.
    costcenter = models.CharField(db_column='COSTCENTER', max_length=255)  # Field name made lowercase.
    chartdate = models.DateTimeField(db_column='CHARTDATE', blank=True, null=True)  # Field name made lowercase.
    cpt_cd = models.CharField(db_column='CPT_CD', max_length=255)  # Field name made lowercase.
    cpt_number = models.PositiveIntegerField(db_column='CPT_NUMBER', blank=True, null=True)  # Field name made lowercase.
    cpt_suffix = models.CharField(db_column='CPT_SUFFIX', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ticket_id_seq = models.PositiveSmallIntegerField(db_column='TICKET_ID_SEQ', blank=True, null=True)  # Field name made lowercase.
    sectionheader = models.CharField(db_column='SECTIONHEADER', max_length=255, blank=True, null=True)  # Field name made lowercase.
    subsectionheader = models.TextField(db_column='SUBSECTIONHEADER', blank=True, null=True)  # Field name made lowercase.
    description = models.CharField(db_column='DESCRIPTION', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CPTEVENTS'


class CurrentMed(models.Model):
    patient_id = models.IntegerField(db_column='PATIENT_ID')  # Field name made lowercase.
    drug_name = models.CharField(db_column='DRUG_NAME', max_length=150, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'CURRENT_MED'


class Datetimeevents(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID', blank=True, null=True)  # Field name made lowercase.
    icustay_id = models.PositiveIntegerField(db_column='ICUSTAY_ID', blank=True, null=True)  # Field name made lowercase.
    itemid = models.PositiveIntegerField(db_column='ITEMID')  # Field name made lowercase.
    charttime = models.DateTimeField(db_column='CHARTTIME')  # Field name made lowercase.
    storetime = models.DateTimeField(db_column='STORETIME')  # Field name made lowercase.
    cgid = models.PositiveSmallIntegerField(db_column='CGID')  # Field name made lowercase.
    value = models.DateTimeField(db_column='VALUE', blank=True, null=True)  # Field name made lowercase.
    valueuom = models.CharField(db_column='VALUEUOM', max_length=255)  # Field name made lowercase.
    warning = models.PositiveIntegerField(db_column='WARNING', blank=True, null=True)  # Field name made lowercase.
    error = models.PositiveIntegerField(db_column='ERROR', blank=True, null=True)  # Field name made lowercase.
    resultstatus = models.CharField(db_column='RESULTSTATUS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    stopped = models.CharField(db_column='STOPPED', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DATETIMEEVENTS'


class DiagnosesIcd(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID')  # Field name made lowercase.
    seq_num = models.PositiveIntegerField(db_column='SEQ_NUM', blank=True, null=True)  # Field name made lowercase.
    icd9_code = models.CharField(db_column='ICD9_CODE', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DIAGNOSES_ICD'


class Drgcodes(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID')  # Field name made lowercase.
    drg_type = models.CharField(db_column='DRG_TYPE', max_length=255)  # Field name made lowercase.
    drg_code = models.CharField(db_column='DRG_CODE', max_length=255)  # Field name made lowercase.
    description = models.TextField(db_column='DESCRIPTION', blank=True, null=True)  # Field name made lowercase.
    drg_severity = models.PositiveIntegerField(db_column='DRG_SEVERITY', blank=True, null=True)  # Field name made lowercase.
    drg_mortality = models.PositiveIntegerField(db_column='DRG_MORTALITY', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'DRGCODES'


class DCpt(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    category = models.PositiveIntegerField(db_column='CATEGORY')  # Field name made lowercase.
    sectionrange = models.CharField(db_column='SECTIONRANGE', max_length=255)  # Field name made lowercase.
    sectionheader = models.CharField(db_column='SECTIONHEADER', max_length=255)  # Field name made lowercase.
    subsectionrange = models.CharField(db_column='SUBSECTIONRANGE', unique=True, max_length=255)  # Field name made lowercase.
    subsectionheader = models.TextField(db_column='SUBSECTIONHEADER')  # Field name made lowercase.
    codesuffix = models.CharField(db_column='CODESUFFIX', max_length=255, blank=True, null=True)  # Field name made lowercase.
    mincodeinsubsection = models.PositiveIntegerField(db_column='MINCODEINSUBSECTION')  # Field name made lowercase.
    maxcodeinsubsection = models.PositiveIntegerField(db_column='MAXCODEINSUBSECTION', unique=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'D_CPT'


class DIcdDiagnoses(models.Model):
    row_id = models.PositiveSmallIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    icd9_code = models.CharField(db_column='ICD9_CODE', unique=True, max_length=255)  # Field name made lowercase.
    short_title = models.CharField(db_column='SHORT_TITLE', max_length=255)  # Field name made lowercase.
    long_title = models.TextField(db_column='LONG_TITLE')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'D_ICD_DIAGNOSES'


class DIcdProcedures(models.Model):
    row_id = models.PositiveSmallIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    icd9_code = models.CharField(db_column='ICD9_CODE', unique=True, max_length=255)  # Field name made lowercase.
    short_title = models.CharField(db_column='SHORT_TITLE', unique=True, max_length=255)  # Field name made lowercase.
    long_title = models.TextField(db_column='LONG_TITLE')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'D_ICD_PROCEDURES'


class DItems(models.Model):
    row_id = models.PositiveSmallIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    itemid = models.PositiveIntegerField(db_column='ITEMID', unique=True)  # Field name made lowercase.
    label = models.TextField(db_column='LABEL', blank=True, null=True)  # Field name made lowercase.
    abbreviation = models.CharField(db_column='ABBREVIATION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dbsource = models.CharField(db_column='DBSOURCE', max_length=255)  # Field name made lowercase.
    linksto = models.CharField(db_column='LINKSTO', max_length=255, blank=True, null=True)  # Field name made lowercase.
    category = models.CharField(db_column='CATEGORY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    unitname = models.CharField(db_column='UNITNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    param_type = models.CharField(db_column='PARAM_TYPE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    conceptid = models.CharField(db_column='CONCEPTID', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'D_ITEMS'


class DLabitems(models.Model):
    row_id = models.PositiveSmallIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    itemid = models.PositiveSmallIntegerField(db_column='ITEMID', unique=True)  # Field name made lowercase.
    label = models.CharField(db_column='LABEL', max_length=255)  # Field name made lowercase.
    fluid = models.CharField(db_column='FLUID', max_length=255)  # Field name made lowercase.
    category = models.CharField(db_column='CATEGORY', max_length=255)  # Field name made lowercase.
    loinc_code = models.CharField(db_column='LOINC_CODE', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'D_LABITEMS'


class HospitalGeneralInfo(models.Model):
    provider_id = models.IntegerField(db_column='Provider_ID', blank=True, null=True)  # Field name made lowercase.
    hospital_name = models.CharField(db_column='Hospital_Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    address = models.CharField(db_column='Address', max_length=255, blank=True, null=True)  # Field name made lowercase.
    city = models.CharField(db_column='City', max_length=255, blank=True, null=True)  # Field name made lowercase.
    state = models.CharField(db_column='State', max_length=2, blank=True, null=True)  # Field name made lowercase.
    zip_code = models.CharField(db_column='ZIP_Code', max_length=5, blank=True, null=True)  # Field name made lowercase.
    county_name = models.CharField(db_column='County_Name', max_length=255, blank=True, null=True)  # Field name made lowercase.
    phone_number = models.IntegerField(db_column='Phone_Number', blank=True, null=True)  # Field name made lowercase.
    hospital_type = models.CharField(db_column='Hospital_Type', max_length=255, blank=True, null=True)  # Field name made lowercase.
    hospital_ownership = models.CharField(db_column='Hospital_Ownership', max_length=255, blank=True, null=True)  # Field name made lowercase.
    emergency_services = models.CharField(db_column='Emergency_Services', max_length=3, blank=True, null=True)  # Field name made lowercase.
    hospital_overall_rating = models.IntegerField(db_column='Hospital_overall_rating', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'HOSPITAL_GENERAL_INFO'


class Icustays(models.Model):
    row_id = models.PositiveSmallIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID')  # Field name made lowercase.
    icustay_id = models.PositiveIntegerField(db_column='ICUSTAY_ID', unique=True)  # Field name made lowercase.
    dbsource = models.CharField(db_column='DBSOURCE', max_length=255)  # Field name made lowercase.
    first_careunit = models.CharField(db_column='FIRST_CAREUNIT', max_length=255)  # Field name made lowercase.
    last_careunit = models.CharField(db_column='LAST_CAREUNIT', max_length=255)  # Field name made lowercase.
    first_wardid = models.PositiveIntegerField(db_column='FIRST_WARDID')  # Field name made lowercase.
    last_wardid = models.PositiveIntegerField(db_column='LAST_WARDID')  # Field name made lowercase.
    intime = models.DateTimeField(db_column='INTIME')  # Field name made lowercase.
    outtime = models.DateTimeField(db_column='OUTTIME', blank=True, null=True)  # Field name made lowercase.
    los = models.FloatField(db_column='LOS', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ICUSTAYS'


class InputeventsCv(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveSmallIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID', blank=True, null=True)  # Field name made lowercase.
    icustay_id = models.PositiveIntegerField(db_column='ICUSTAY_ID', blank=True, null=True)  # Field name made lowercase.
    charttime = models.DateTimeField(db_column='CHARTTIME')  # Field name made lowercase.
    itemid = models.PositiveSmallIntegerField(db_column='ITEMID')  # Field name made lowercase.
    amount = models.FloatField(db_column='AMOUNT', blank=True, null=True)  # Field name made lowercase.
    amountuom = models.CharField(db_column='AMOUNTUOM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    rate = models.FloatField(db_column='RATE', blank=True, null=True)  # Field name made lowercase.
    rateuom = models.CharField(db_column='RATEUOM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    storetime = models.DateTimeField(db_column='STORETIME')  # Field name made lowercase.
    cgid = models.PositiveSmallIntegerField(db_column='CGID', blank=True, null=True)  # Field name made lowercase.
    orderid = models.PositiveIntegerField(db_column='ORDERID')  # Field name made lowercase.
    linkorderid = models.PositiveIntegerField(db_column='LINKORDERID')  # Field name made lowercase.
    stopped = models.CharField(db_column='STOPPED', max_length=255, blank=True, null=True)  # Field name made lowercase.
    newbottle = models.PositiveIntegerField(db_column='NEWBOTTLE', blank=True, null=True)  # Field name made lowercase.
    originalamount = models.FloatField(db_column='ORIGINALAMOUNT', blank=True, null=True)  # Field name made lowercase.
    originalamountuom = models.CharField(db_column='ORIGINALAMOUNTUOM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    originalroute = models.CharField(db_column='ORIGINALROUTE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    originalrate = models.FloatField(db_column='ORIGINALRATE', blank=True, null=True)  # Field name made lowercase.
    originalrateuom = models.CharField(db_column='ORIGINALRATEUOM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    originalsite = models.CharField(db_column='ORIGINALSITE', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'INPUTEVENTS_CV'


class InputeventsMv(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID')  # Field name made lowercase.
    icustay_id = models.PositiveIntegerField(db_column='ICUSTAY_ID', blank=True, null=True)  # Field name made lowercase.
    starttime = models.DateTimeField(db_column='STARTTIME')  # Field name made lowercase.
    endtime = models.DateTimeField(db_column='ENDTIME')  # Field name made lowercase.
    itemid = models.PositiveIntegerField(db_column='ITEMID')  # Field name made lowercase.
    amount = models.FloatField(db_column='AMOUNT')  # Field name made lowercase.
    amountuom = models.CharField(db_column='AMOUNTUOM', max_length=255)  # Field name made lowercase.
    rate = models.FloatField(db_column='RATE', blank=True, null=True)  # Field name made lowercase.
    rateuom = models.CharField(db_column='RATEUOM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    storetime = models.DateTimeField(db_column='STORETIME')  # Field name made lowercase.
    cgid = models.PositiveSmallIntegerField(db_column='CGID')  # Field name made lowercase.
    orderid = models.PositiveIntegerField(db_column='ORDERID')  # Field name made lowercase.
    linkorderid = models.PositiveIntegerField(db_column='LINKORDERID')  # Field name made lowercase.
    ordercategoryname = models.CharField(db_column='ORDERCATEGORYNAME', max_length=255)  # Field name made lowercase.
    secondaryordercategoryname = models.CharField(db_column='SECONDARYORDERCATEGORYNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ordercomponenttypedescription = models.CharField(db_column='ORDERCOMPONENTTYPEDESCRIPTION', max_length=255)  # Field name made lowercase.
    ordercategorydescription = models.CharField(db_column='ORDERCATEGORYDESCRIPTION', max_length=255)  # Field name made lowercase.
    patientweight = models.FloatField(db_column='PATIENTWEIGHT')  # Field name made lowercase.
    totalamount = models.FloatField(db_column='TOTALAMOUNT', blank=True, null=True)  # Field name made lowercase.
    totalamountuom = models.CharField(db_column='TOTALAMOUNTUOM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    isopenbag = models.PositiveIntegerField(db_column='ISOPENBAG')  # Field name made lowercase.
    continueinnextdept = models.PositiveIntegerField(db_column='CONTINUEINNEXTDEPT')  # Field name made lowercase.
    cancelreason = models.PositiveIntegerField(db_column='CANCELREASON')  # Field name made lowercase.
    statusdescription = models.CharField(db_column='STATUSDESCRIPTION', max_length=255)  # Field name made lowercase.
    comments_editedby = models.CharField(db_column='COMMENTS_EDITEDBY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    comments_canceledby = models.CharField(db_column='COMMENTS_CANCELEDBY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    comments_date = models.DateTimeField(db_column='COMMENTS_DATE', blank=True, null=True)  # Field name made lowercase.
    originalamount = models.FloatField(db_column='ORIGINALAMOUNT')  # Field name made lowercase.
    originalrate = models.FloatField(db_column='ORIGINALRATE')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'INPUTEVENTS_MV'


class Labevents(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID', blank=True, null=True)  # Field name made lowercase.
    itemid = models.PositiveSmallIntegerField(db_column='ITEMID')  # Field name made lowercase.
    charttime = models.DateTimeField(db_column='CHARTTIME')  # Field name made lowercase.
    value = models.TextField(db_column='VALUE', blank=True, null=True)  # Field name made lowercase.
    valuenum = models.FloatField(db_column='VALUENUM', blank=True, null=True)  # Field name made lowercase.
    valueuom = models.CharField(db_column='VALUEUOM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    flag = models.CharField(db_column='FLAG', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'LABEVENTS'


class Microbiologyevents(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID', blank=True, null=True)  # Field name made lowercase.
    chartdate = models.DateTimeField(db_column='CHARTDATE')  # Field name made lowercase.
    charttime = models.DateTimeField(db_column='CHARTTIME', blank=True, null=True)  # Field name made lowercase.
    spec_itemid = models.PositiveIntegerField(db_column='SPEC_ITEMID', blank=True, null=True)  # Field name made lowercase.
    spec_type_desc = models.CharField(db_column='SPEC_TYPE_DESC', max_length=255)  # Field name made lowercase.
    org_itemid = models.PositiveIntegerField(db_column='ORG_ITEMID', blank=True, null=True)  # Field name made lowercase.
    org_name = models.CharField(db_column='ORG_NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    isolate_num = models.PositiveIntegerField(db_column='ISOLATE_NUM', blank=True, null=True)  # Field name made lowercase.
    ab_itemid = models.PositiveIntegerField(db_column='AB_ITEMID', blank=True, null=True)  # Field name made lowercase.
    ab_name = models.CharField(db_column='AB_NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dilution_text = models.CharField(db_column='DILUTION_TEXT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dilution_comparison = models.CharField(db_column='DILUTION_COMPARISON', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dilution_value = models.PositiveSmallIntegerField(db_column='DILUTION_VALUE', blank=True, null=True)  # Field name made lowercase.
    interpretation = models.CharField(db_column='INTERPRETATION', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'MICROBIOLOGYEVENTS'


class Noteevents(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID', blank=True, null=True)  # Field name made lowercase.
    chartdate = models.DateField(db_column='CHARTDATE')  # Field name made lowercase.
    charttime = models.DateTimeField(db_column='CHARTTIME', blank=True, null=True)  # Field name made lowercase.
    storetime = models.DateTimeField(db_column='STORETIME', blank=True, null=True)  # Field name made lowercase.
    category = models.CharField(db_column='CATEGORY', max_length=255)  # Field name made lowercase.
    description = models.CharField(db_column='DESCRIPTION', max_length=255)  # Field name made lowercase.
    cgid = models.PositiveSmallIntegerField(db_column='CGID', blank=True, null=True)  # Field name made lowercase.
    iserror = models.PositiveIntegerField(db_column='ISERROR', blank=True, null=True)  # Field name made lowercase.
    text = models.TextField(db_column='TEXT', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'NOTEEVENTS'


class Outputevents(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID', blank=True, null=True)  # Field name made lowercase.
    icustay_id = models.PositiveIntegerField(db_column='ICUSTAY_ID', blank=True, null=True)  # Field name made lowercase.
    charttime = models.DateTimeField(db_column='CHARTTIME')  # Field name made lowercase.
    itemid = models.PositiveIntegerField(db_column='ITEMID')  # Field name made lowercase.
    value = models.FloatField(db_column='VALUE', blank=True, null=True)  # Field name made lowercase.
    valueuom = models.CharField(db_column='VALUEUOM', max_length=255, blank=True, null=True)  # Field name made lowercase.
    storetime = models.DateTimeField(db_column='STORETIME')  # Field name made lowercase.
    cgid = models.PositiveSmallIntegerField(db_column='CGID')  # Field name made lowercase.
    stopped = models.CharField(db_column='STOPPED', max_length=255, blank=True, null=True)  # Field name made lowercase.
    newbottle = models.CharField(db_column='NEWBOTTLE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    iserror = models.CharField(db_column='ISERROR', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'OUTPUTEVENTS'


class Patients(models.Model):
    row_id = models.PositiveSmallIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID', unique=True)  # Field name made lowercase.
    gender = models.CharField(db_column='GENDER', max_length=255)  # Field name made lowercase.
    dob = models.CharField(db_column='DOB', max_length=255)  # Field name made lowercase.
    dod = models.DateTimeField(db_column='DOD', blank=True, null=True)  # Field name made lowercase.
    dod_hosp = models.DateTimeField(db_column='DOD_HOSP', blank=True, null=True)  # Field name made lowercase.
    dod_ssn = models.DateTimeField(db_column='DOD_SSN', blank=True, null=True)  # Field name made lowercase.
    expire_flag = models.PositiveIntegerField(db_column='EXPIRE_FLAG')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PATIENTS'


class Prescriptions(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID')  # Field name made lowercase.
    icustay_id = models.PositiveIntegerField(db_column='ICUSTAY_ID', blank=True, null=True)  # Field name made lowercase.
    startdate = models.DateTimeField(db_column='STARTDATE', blank=True, null=True)  # Field name made lowercase.
    enddate = models.DateTimeField(db_column='ENDDATE', blank=True, null=True)  # Field name made lowercase.
    drug_type = models.CharField(db_column='DRUG_TYPE', max_length=255)  # Field name made lowercase.
    drug = models.CharField(db_column='DRUG', max_length=255, blank=True, null=True)  # Field name made lowercase.
    drug_name_poe = models.CharField(db_column='DRUG_NAME_POE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    drug_name_generic = models.CharField(db_column='DRUG_NAME_GENERIC', max_length=255, blank=True, null=True)  # Field name made lowercase.
    formulary_drug_cd = models.CharField(db_column='FORMULARY_DRUG_CD', max_length=255, blank=True, null=True)  # Field name made lowercase.
    gsn = models.TextField(db_column='GSN', blank=True, null=True)  # Field name made lowercase.
    ndc = models.CharField(db_column='NDC', max_length=255, blank=True, null=True)  # Field name made lowercase.
    prod_strength = models.CharField(db_column='PROD_STRENGTH', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dose_val_rx = models.CharField(db_column='DOSE_VAL_RX', max_length=255, blank=True, null=True)  # Field name made lowercase.
    dose_unit_rx = models.CharField(db_column='DOSE_UNIT_RX', max_length=255, blank=True, null=True)  # Field name made lowercase.
    form_val_disp = models.CharField(db_column='FORM_VAL_DISP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    form_unit_disp = models.CharField(db_column='FORM_UNIT_DISP', max_length=255, blank=True, null=True)  # Field name made lowercase.
    route = models.CharField(db_column='ROUTE', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PRESCRIPTIONS'


class ProcedureeventsMv(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID')  # Field name made lowercase.
    icustay_id = models.PositiveIntegerField(db_column='ICUSTAY_ID', blank=True, null=True)  # Field name made lowercase.
    starttime = models.DateTimeField(db_column='STARTTIME')  # Field name made lowercase.
    endtime = models.DateTimeField(db_column='ENDTIME')  # Field name made lowercase.
    itemid = models.PositiveIntegerField(db_column='ITEMID')  # Field name made lowercase.
    value = models.FloatField(db_column='VALUE')  # Field name made lowercase.
    valueuom = models.CharField(db_column='VALUEUOM', max_length=255)  # Field name made lowercase.
    location = models.CharField(db_column='LOCATION', max_length=255, blank=True, null=True)  # Field name made lowercase.
    locationcategory = models.CharField(db_column='LOCATIONCATEGORY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    storetime = models.DateTimeField(db_column='STORETIME')  # Field name made lowercase.
    cgid = models.PositiveSmallIntegerField(db_column='CGID')  # Field name made lowercase.
    orderid = models.PositiveIntegerField(db_column='ORDERID', unique=True)  # Field name made lowercase.
    linkorderid = models.PositiveIntegerField(db_column='LINKORDERID')  # Field name made lowercase.
    ordercategoryname = models.CharField(db_column='ORDERCATEGORYNAME', max_length=255)  # Field name made lowercase.
    secondaryordercategoryname = models.CharField(db_column='SECONDARYORDERCATEGORYNAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    ordercategorydescription = models.CharField(db_column='ORDERCATEGORYDESCRIPTION', max_length=255)  # Field name made lowercase.
    isopenbag = models.PositiveIntegerField(db_column='ISOPENBAG')  # Field name made lowercase.
    continueinnextdept = models.PositiveIntegerField(db_column='CONTINUEINNEXTDEPT')  # Field name made lowercase.
    cancelreason = models.PositiveIntegerField(db_column='CANCELREASON')  # Field name made lowercase.
    statusdescription = models.CharField(db_column='STATUSDESCRIPTION', max_length=255)  # Field name made lowercase.
    comments_editedby = models.CharField(db_column='COMMENTS_EDITEDBY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    comments_canceledby = models.CharField(db_column='COMMENTS_CANCELEDBY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    comments_date = models.DateTimeField(db_column='COMMENTS_DATE', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROCEDUREEVENTS_MV'


class ProceduresIcd(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID')  # Field name made lowercase.
    seq_num = models.PositiveIntegerField(db_column='SEQ_NUM')  # Field name made lowercase.
    icd9_code = models.CharField(db_column='ICD9_CODE', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'PROCEDURES_ICD'


class RawCallout(models.Model):
    row_id = models.IntegerField(db_column='ROW_ID', blank=True, null=True)  # Field name made lowercase.
    subject_id = models.IntegerField(db_column='SUBJECT_ID', blank=True, null=True)  # Field name made lowercase.
    hadm_id = models.IntegerField(db_column='HADM_ID', blank=True, null=True)  # Field name made lowercase.
    submit_wardid = models.IntegerField(db_column='SUBMIT_WARDID', blank=True, null=True)  # Field name made lowercase.
    submit_careunit = models.CharField(db_column='SUBMIT_CAREUNIT', max_length=15, blank=True, null=True)  # Field name made lowercase.
    curr_wardid = models.IntegerField(db_column='CURR_WARDID', blank=True, null=True)  # Field name made lowercase.
    curr_careunit = models.CharField(db_column='CURR_CAREUNIT', max_length=15, blank=True, null=True)  # Field name made lowercase.
    callout_wardid = models.IntegerField(db_column='CALLOUT_WARDID', blank=True, null=True)  # Field name made lowercase.
    callout_service = models.CharField(db_column='CALLOUT_SERVICE', max_length=10, blank=True, null=True)  # Field name made lowercase.
    request_tele = models.SmallIntegerField(db_column='REQUEST_TELE', blank=True, null=True)  # Field name made lowercase.
    request_resp = models.SmallIntegerField(db_column='REQUEST_RESP', blank=True, null=True)  # Field name made lowercase.
    request_cdiff = models.SmallIntegerField(db_column='REQUEST_CDIFF', blank=True, null=True)  # Field name made lowercase.
    request_mrsa = models.SmallIntegerField(db_column='REQUEST_MRSA', blank=True, null=True)  # Field name made lowercase.
    request_vre = models.SmallIntegerField(db_column='REQUEST_VRE', blank=True, null=True)  # Field name made lowercase.
    callout_status = models.CharField(db_column='CALLOUT_STATUS', max_length=20, blank=True, null=True)  # Field name made lowercase.
    callout_outcome = models.CharField(db_column='CALLOUT_OUTCOME', max_length=20, blank=True, null=True)  # Field name made lowercase.
    discharge_wardid = models.IntegerField(db_column='DISCHARGE_WARDID', blank=True, null=True)  # Field name made lowercase.
    acknowledge_status = models.CharField(db_column='ACKNOWLEDGE_STATUS', max_length=20, blank=True, null=True)  # Field name made lowercase.
    createtime = models.DateTimeField(db_column='CREATETIME')  # Field name made lowercase.
    updatetime = models.DateTimeField(db_column='UPDATETIME')  # Field name made lowercase.
    acknowledgetime = models.DateTimeField(db_column='ACKNOWLEDGETIME')  # Field name made lowercase.
    outcometime = models.DateTimeField(db_column='OUTCOMETIME')  # Field name made lowercase.
    firstreservationtime = models.DateTimeField(db_column='FIRSTRESERVATIONTIME')  # Field name made lowercase.
    currentreservationtime = models.DateTimeField(db_column='CURRENTRESERVATIONTIME')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'RAW_CALLOUT'


class Services(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID')  # Field name made lowercase.
    transfertime = models.DateTimeField(db_column='TRANSFERTIME')  # Field name made lowercase.
    prev_service = models.CharField(db_column='PREV_SERVICE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    curr_service = models.CharField(db_column='CURR_SERVICE', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SERVICES'


class Staff(models.Model):
   # id = models.IntegerField(db_column='ID', blank=True, null=True)  # Field name made lowercase.
    occupation = models.CharField(db_column='OCCUPATION', max_length=15)  # Field name made lowercase.
    details = models.CharField(db_column='DETAILS', max_length=30, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'STAFF'


class Subjects(models.Model):
    patient_id = models.IntegerField(db_column='PATIENT_ID', blank=True, null=True)  # Field name made lowercase.
    gender = models.CharField(db_column='GENDER', max_length=6, blank=True, null=True)  # Field name made lowercase.
    dob = models.DateField(db_column='DOB', blank=True, null=True)  # Field name made lowercase.
    dod = models.DateField(db_column='DOD', blank=True, null=True)  # Field name made lowercase.
    dod_hosp = models.DateField(db_column='DOD_HOSP', blank=True, null=True)  # Field name made lowercase.
    dead = models.IntegerField(db_column='DEAD', blank=True, null=True)  # Field name made lowercase.
    insurance = models.CharField(db_column='INSURANCE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    blood_type = models.CharField(db_column='BLOOD_TYPE', max_length=3, blank=True, null=True)  # Field name made lowercase.
    provider_id = models.IntegerField(db_column='PROVIDER_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'SUBJECTS'


class Transfers(models.Model):
    row_id = models.PositiveIntegerField(db_column='ROW_ID', unique=True)  # Field name made lowercase.
    subject_id = models.PositiveIntegerField(db_column='SUBJECT_ID')  # Field name made lowercase.
    hadm_id = models.PositiveIntegerField(db_column='HADM_ID')  # Field name made lowercase.
    icustay_id = models.PositiveIntegerField(db_column='ICUSTAY_ID', blank=True, null=True)  # Field name made lowercase.
    dbsource = models.CharField(db_column='DBSOURCE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    eventtype = models.CharField(db_column='EVENTTYPE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    prev_careunit = models.CharField(db_column='PREV_CAREUNIT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    curr_careunit = models.CharField(db_column='CURR_CAREUNIT', max_length=255, blank=True, null=True)  # Field name made lowercase.
    prev_wardid = models.PositiveIntegerField(db_column='PREV_WARDID', blank=True, null=True)  # Field name made lowercase.
    curr_wardid = models.PositiveIntegerField(db_column='CURR_WARDID', blank=True, null=True)  # Field name made lowercase.
    intime = models.DateTimeField(db_column='INTIME', blank=True, null=True)  # Field name made lowercase.
    outtime = models.DateTimeField(db_column='OUTTIME', blank=True, null=True)  # Field name made lowercase.
    los = models.FloatField(db_column='LOS', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'TRANSFERS'


class Users(models.Model):
    id = models.AutoField(db_column='ID', primary_key=True)  # Field name made lowercase.
    username = models.CharField(db_column='USERNAME', max_length=150)  # Field name made lowercase.
    password = models.CharField(db_column='PASSWORD', max_length=150, blank=True, null=True)  # Field name made lowercase.
    user_privileges = models.CharField(db_column='USER_PRIVILEGES', max_length=150)  # Field name made lowercase.
    time_created = models.DateTimeField(db_column='TIME_CREATED')  # Field name made lowercase.

    class Meta:
        managed = True
        db_table = 'USERS'

class TblUser(models.Model):
    user_id = models.BigAutoField(primary_key=True)
    user_name = models.CharField(max_length=45, blank=True, null=True)
    user_username = models.CharField(max_length=45, blank=True, null=True)
    user_password = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tbl_user'

class UserProfile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __unicode__(self):
        return self.user.username

    class Meta:
        managed = True
        

class UserAge(models.Model):
    age = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'user_age'
