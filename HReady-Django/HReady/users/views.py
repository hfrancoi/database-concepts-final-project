from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import CreateView, TemplateView
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required


from .models import CustomUser

from .forms import FirstResponderSignupForm, HospitalAdminSignupForm



# Create your views here.

def home(request):
    return render(request, 'main/home.html')



class SignUpView(TemplateView):
    template_name = 'registration/signup.html'




class FirstResponderSignupView(CreateView):
    model = CustomUser
    form_class = FirstResponderSignupForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'first_responder'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('tracking:hsearch')


class HospitalAdminSignupView(CreateView):
    model = CustomUser
    form_class = HospitalAdminSignupForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'hospital_admin'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('system:dashboard')


