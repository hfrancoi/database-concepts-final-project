from django.test import TestCase
from .models import CustomUser, Hospital_Admin, First_Responder

# Create your tests here.

class UserTestCase(test.TestCase):
    def setUp(self):
        First_Responder.objects.create(username="test1", email="test1@example.com", password="test1test1")
        First_Responder.objects.create(username="test2", email="test2@example.com", password="test2test2")

    def test_user_category(self):
        first_responder = CustomUser.objects.get()
        hospital_admin = CustomUser.objects.get()
        self.assertEqual(first_responder.is_first_responder, True)
        self.assertEqual(first_responder.is_hospital_admin, False)
        self.assertEqual(hospital_admin.is_first_responder, False)
        self.assertEqual(hospital_admin.is_hospital_admin, True)
