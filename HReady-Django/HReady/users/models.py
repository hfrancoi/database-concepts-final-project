from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.
class CustomUser(AbstractUser):
    is_first_responder = models.BooleanField(default=False)
    is_hospital_admin  = models.BooleanField(default=False)


class First_Responder(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, primary_key=True)
    

class Hospital_Admin(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, primary_key=True)
    # hospital = models.ForeignKey(Hospital)
