from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.db import transaction


from .models import CustomUser


class FirstResponderSignupForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = CustomUser

    @transaction.atomic
    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_first_responder = True
        if commit:
            user.save()
        return user



class HospitalAdminSignupForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = CustomUser


    @transaction.atomic
    def save(self, commit=True):
        user = super().save(commit=False)
        user.is_hospital_admin = True
        if commit:
            user.save()
        return user
