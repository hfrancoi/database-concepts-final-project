from django.shortcuts import redirect, render
from django.urls import path, include
from django.contrib.auth import views as auth_views
from users import views as user_views

app_name = 'users'

urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('signup/', user_views.SignUpView.as_view(), name='signup'),
    path('signup/first_responder', user_views.FirstResponderSignupView.as_view(), name='firstresponder_signup'),
    path('signup/hospital', user_views.HospitalAdminSignupView.as_view(), name='hospitaladmin_signup')
]
