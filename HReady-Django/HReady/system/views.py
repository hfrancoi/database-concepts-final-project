from django.shortcuts import render
from django.views.generic import (ListView, TemplateView, DetailView, UpdateView, CreateView, View)
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

from .models import Staff, Subjects, HospitalGeneralInfo, Cases

from .forms import CaseUpdateForm

# Create your views here.
decorators = [login_required]


######################################################
###################Case Views#########################
######################################################

class CaseListView(ListView):
    """ Dashboard for hospital administrator """
    model = Cases
    template_name = 'system/case_list.html'
    paginate_by = 40

class CaseDetailView(DetailView):
    """ Individual view for a case """
    queryset = Cases.objects.order_by('-updated')
    template_name = 'system/case_detail.html'



class CaseUpdateView(UpdateView):
    model = Cases
    template_name = 'system/case_detail.html'
    

class CaseCreateView(CreateView):
    model = Cases
    fields = ['subject_id']
    template_name = 'system/case_create.html'


######################################################
###################Subject Views######################
######################################################

class SubjectListView(ListView):
    queryset = Subjects.objects.filter(dob__year__gte=1982, dob__year__lte=2018).order_by('patient_id')
    template_name = 'system/subject_list.html'
    paginate_by = 100
    context_object_name = 'patients'


class SubjectDetailView(DetailView):
    model = Subjects
    template_name = 'system/subject_detail.html'

class SubjectCreateView(CreateView):
    model = Subjects
    fields = ['gender', 'dob','dead', 'insurance', 'blood_type']
    template_name = 'subject_create.html'

class SubjectUpdateView(UpdateView):
    model = Subjects
    template_name = 'system/subject_update.html'
    form_class = CaseUpdateForm

######################################################
###################CaseEvent Views####################
######################################################
