# Generated by Django 2.0.9 on 2018-12-02 18:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('system', '0003_cases_hospitalgeneralinfo_staff_subjects'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hospitalgeneralinfo',
            name='hospital_overall_rating',
        ),
    ]
