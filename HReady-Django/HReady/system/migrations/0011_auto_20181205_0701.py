# Generated by Django 2.0.9 on 2018-12-05 07:01

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('system', '0010_auto_20181205_0324'),
    ]

    operations = [
        migrations.CreateModel(
            name='CaseEvents',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('hadm_id', models.IntegerField(db_column='HADM_ID')),
                ('event_num', models.IntegerField(db_column='EVENT_NUM')),
                ('cgid', models.IntegerField(db_column='CGID')),
                ('description', models.CharField(db_column='DESCRIPTION', max_length=255)),
            ],
            options={
                'managed': True,
                'db_table': 'CASE_EVENTS',
            },
        ),
        migrations.AddField(
            model_name='cases',
            name='hready_id',
            field=models.AutoField(db_column='HREADY_ID', default=1, primary_key=True, serialize=False),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='cases',
            name='closed',
            field=models.IntegerField(db_column='CLOSED', default=0),
        ),
        migrations.AlterField(
            model_name='cases',
            name='expire_flag',
            field=models.IntegerField(db_column='EXPIRE_FLAG', default=0),
        ),
        migrations.AlterField(
            model_name='cases',
            name='hadm_id',
            field=models.IntegerField(db_column='HADM_ID'),
        ),
        migrations.AlterField(
            model_name='cases',
            name='starttime',
            field=models.DateTimeField(auto_now_add=True, db_column='STARTTIME'),
        ),
        migrations.AlterField(
            model_name='cases',
            name='subject_id',
            field=models.ForeignKey(db_column='SUBJECT_ID', default=1, on_delete=django.db.models.deletion.CASCADE, to='system.Subjects'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='cases',
            name='updated',
            field=models.DateTimeField(auto_now=True, db_column='UPDATED', default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='caseevents',
            name='hready_id',
            field=models.ForeignKey(db_column='HREADY_ID', on_delete=django.db.models.deletion.CASCADE, to='system.Cases'),
        ),
    ]
