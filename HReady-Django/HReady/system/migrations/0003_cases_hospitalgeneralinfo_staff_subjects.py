# Generated by Django 2.0.9 on 2018-12-02 18:25

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('system', '0002_auto_20181202_1825'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cases',
            fields=[
                ('case_id', models.AutoField(db_column='CASE_ID', primary_key=True, serialize=False)),
                ('patient_id', models.IntegerField(blank=True, db_column='PATIENT_ID', null=True)),
                ('cpt_number', models.IntegerField(blank=True, db_column='CPT_NUMBER', null=True)),
                ('cgid', models.IntegerField(blank=True, db_column='CGID', null=True)),
                ('diagnosis', models.CharField(blank=True, db_column='DIAGNOSIS', max_length=255, null=True)),
                ('starttime', models.DateTimeField(db_column='STARTTIME')),
                ('endtime', models.DateTimeField(blank=True, db_column='ENDTIME', null=True)),
                ('provider_id', models.IntegerField(blank=True, db_column='PROVIDER_ID', null=True)),
                ('closed', models.IntegerField(blank=True, db_column='CLOSED', null=True)),
                ('updated', models.DateTimeField(blank=True, db_column='UPDATED', null=True)),
            ],
            options={
                'managed': True,
                'db_table': 'CASES',
            },
        ),
        migrations.CreateModel(
            name='HospitalGeneralInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('provider_id', models.IntegerField(blank=True, db_column='Provider_ID', null=True)),
                ('hospital_name', models.CharField(blank=True, db_column='Hospital_Name', max_length=255, null=True)),
                ('address', models.CharField(blank=True, db_column='Address', max_length=255, null=True)),
                ('city', models.CharField(blank=True, db_column='City', max_length=255, null=True)),
                ('state', models.CharField(blank=True, db_column='State', max_length=2, null=True)),
                ('zip_code', models.CharField(blank=True, db_column='ZIP_Code', max_length=5, null=True)),
                ('county_name', models.CharField(blank=True, db_column='County_Name', max_length=255, null=True)),
                ('phone_number', models.IntegerField(blank=True, db_column='Phone_Number', null=True)),
                ('hospital_type', models.CharField(blank=True, db_column='Hospital_Type', max_length=255, null=True)),
                ('hospital_ownership', models.CharField(blank=True, db_column='Hospital_Ownership', max_length=255, null=True)),
                ('emergency_services', models.CharField(blank=True, db_column='Emergency_Services', max_length=3, null=True)),
                ('hospital_overall_rating', models.IntegerField(blank=True, db_column='Hospital_overall_rating', null=True)),
            ],
            options={
                'managed': True,
                'db_table': 'HOSPITAL_GENERAL_INFO',
            },
        ),
        migrations.CreateModel(
            name='Staff',
            fields=[
                ('id', models.IntegerField(db_column='ID', primary_key=True, serialize=False)),
                ('occupation', models.CharField(db_column='OCCUPATION', max_length=15)),
                ('details', models.CharField(blank=True, db_column='DETAILS', max_length=30, null=True)),
                ('provider_id', models.IntegerField(blank=True, db_column='PROVIDER_ID', null=True)),
            ],
            options={
                'managed': True,
                'db_table': 'STAFF',
            },
        ),
        migrations.CreateModel(
            name='Subjects',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('patient_id', models.PositiveIntegerField(blank=True, db_column='PATIENT_ID', null=True)),
                ('gender', models.CharField(blank=True, db_column='GENDER', max_length=6, null=True)),
                ('dob', models.DateField(blank=True, db_column='DOB', null=True)),
                ('dod', models.DateField(blank=True, db_column='DOD', null=True)),
                ('dod_hosp', models.DateField(blank=True, db_column='DOD_HOSP', null=True)),
                ('dead', models.IntegerField(blank=True, db_column='DEAD', null=True)),
                ('insurance', models.CharField(blank=True, db_column='INSURANCE', max_length=255, null=True)),
                ('blood_type', models.CharField(blank=True, db_column='BLOOD_TYPE', max_length=3, null=True)),
                ('provider_id', models.IntegerField(blank=True, db_column='PROVIDER_ID', null=True)),
            ],
            options={
                'managed': True,
                'db_table': 'SUBJECTS',
            },
        ),
    ]
