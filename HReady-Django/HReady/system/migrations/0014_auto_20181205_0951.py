# Generated by Django 2.0.9 on 2018-12-05 09:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('system', '0013_auto_20181205_0707'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hospitalgeneralinfo',
            name='address',
            field=models.CharField(blank=True, db_column='ADDRESS', max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='hospitalgeneralinfo',
            name='city',
            field=models.CharField(blank=True, db_column='CITY', max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='hospitalgeneralinfo',
            name='county_name',
            field=models.CharField(blank=True, db_column='COUNTY', max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='hospitalgeneralinfo',
            name='emergency_services',
            field=models.CharField(blank=True, db_column='EMERGENCY_SERVICES', max_length=3, null=True),
        ),
        migrations.AlterField(
            model_name='hospitalgeneralinfo',
            name='hospital_name',
            field=models.CharField(blank=True, db_column='HOSPITAL_NAME', max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='hospitalgeneralinfo',
            name='hospital_type',
            field=models.CharField(blank=True, db_column='TYPE', max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='hospitalgeneralinfo',
            name='phone_number',
            field=models.IntegerField(blank=True, db_column='PHONE_NUM', null=True),
        ),
        migrations.AlterField(
            model_name='hospitalgeneralinfo',
            name='provider_id',
            field=models.IntegerField(blank=True, db_column='PROVIDER_ID', null=True),
        ),
        migrations.AlterField(
            model_name='hospitalgeneralinfo',
            name='state',
            field=models.CharField(blank=True, db_column='STATE', max_length=2, null=True),
        ),
        migrations.AlterField(
            model_name='hospitalgeneralinfo',
            name='zip_code',
            field=models.CharField(blank=True, db_column='ZIP_CODE', max_length=5, null=True),
        ),
        migrations.AlterModelTable(
            name='hospitalgeneralinfo',
            table='PROVIDERS',
        ),
    ]
