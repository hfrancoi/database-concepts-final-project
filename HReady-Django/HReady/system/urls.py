from django.shortcuts import redirect, render
from django.urls import path, include
from .views import *

app_name = 'system'

urlpatterns = [
    path('', include([
        path('subjects/', SubjectListView.as_view(), name='subjects'),
        path('subjects/create', SubjectCreateView.as_view(), name='subject-create'),
        path('subjects/id/<int:pk>/', SubjectDetailView.as_view(), name='subject-detail'),
        path('subjects/update/<int:pk>/', SubjectUpdateView.as_view(), name='subject-update'),
        path('cases/', CaseListView.as_view(), name='dashboard'),
        path('cases/create', CaseCreateView.as_view(), name='case_create'),
        path('cases/id/<int:pk>/', CaseDetailView.as_view(), name='case-detail'),
        path('cases/update/<int:pk>/', CaseUpdateView.as_view(), name='case-update')
    ]))
]
