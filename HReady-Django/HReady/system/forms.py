from django import forms
from django.db import transaction

from .models import Cases

class CaseUpdateForm(forms.ModelForm):
    class Meta:
        model = Cases
        fields = ['hadm_id', 'provider', 'subject', 'expire_flag']



