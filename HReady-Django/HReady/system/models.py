from django.db import models
from django.urls import reverse

from users.models import Hospital_Admin, First_Responder

# Create your models here.

# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.

class Staff(models.Model):
    id = models.IntegerField(db_column='ID', primary_key=True)  # Field name made lowercase.
    occupation = models.CharField(db_column='OCCUPATION', max_length=15)  # Field name made lowercase.
    details = models.CharField(db_column='DETAILS', max_length=30, blank=True, null=True)  # Field name made lowercase.
    provider_id = models.IntegerField(db_column='PROVIDER_ID', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = True
        db_table = 'STAFF'

class HospitalGeneralInfo(models.Model):
    provider_id = models.AutoField(db_column='PROVIDER_ID', primary_key=True)  # Field name made lowercase.
    hospital_name = models.CharField(db_column='HOSPITAL_NAME', max_length=255, blank=True, null=True)  # Field name made lowercase.
    address = models.CharField(db_column='ADDRESS', max_length=255, blank=True, null=True)  # Field name made lowercase.
    city = models.CharField(db_column='CITY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    state = models.CharField(db_column='STATE', max_length=2, blank=True, null=True)  # Field name made lowercase.
    zip_code = models.CharField(db_column='ZIP_CODE', max_length=5, blank=True, null=True)  # Field name made lowercase.
    county_name = models.CharField(db_column='COUNTY', max_length=255, blank=True, null=True)  # Field name made lowercase.
    phone_number = models.IntegerField(db_column='PHONE_NUM', blank=True, null=True)  # Field name made lowercase.
    hospital_type = models.CharField(db_column='TYPE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    emergency_services = models.CharField(db_column='EMERGENCY_SERVICES', max_length=3, blank=True, null=True)  # Field name made lowercase.
    #hospital_admin = models.OneToOneField(Hospital_Admin, blank=True, null=True, on_delete=models.SET_NULL)
    #hospital_overall_rating = models.IntegerField(db_column='Hospital_overall_rating', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = True
        db_table = 'PROVIDERS'


class Subjects(models.Model):
    patient_id = models.AutoField(db_column='PATIENT_ID', primary_key=True)  # Field name made lowercase.
    patient_name = models.CharField(db_column='PATIENT_NAME', max_length=255)
    gender = models.CharField(db_column='GENDER', max_length=6, blank=True, null=True)  # Field name made lowercase.
    dob = models.DateField(db_column='DOB', blank=True, null=True)  # Field name made lowercase.
    dod = models.DateField(db_column='DOD', blank=True, null=True)  # Field name made lowercase.
    dod_hosp = models.DateField(db_column='DOD_HOSP', blank=True, null=True)  # Field name made lowercase.
    dead = models.IntegerField(db_column='DEAD', blank=True, null=True)  # Field name made lowercase.
    insurance = models.CharField(db_column='INSURANCE', max_length=255, blank=True, null=True)  # Field name made lowercase.
    blood_type = models.CharField(db_column='BLOOD_TYPE', max_length=3, blank=True, null=True)  # Field name made lowercase.

    def get_absolute_url(self):
        return reverse('system:subject-detail', kwargs={'pk': self.pk})

    class Meta:
        managed = True
        db_table = 'SUBJECTS'

class Cases(models.Model):
    hready_id = models.AutoField(db_column='HREADY_ID', primary_key=True)
    hadm_id = models.IntegerField(db_column='HADM_ID')  # Field name made lowercase.
    subject = models.ForeignKey(Subjects, db_column='SUBJECT_ID', on_delete=models.CASCADE)  # Field name made lowercase.
    provider = models.ForeignKey(HospitalGeneralInfo, db_column='PROVIDER_ID', null=True, on_delete=models.SET_NULL)
    starttime = models.DateTimeField(db_column='STARTTIME', auto_now_add=True)  # Field name made lowercase.
    endtime = models.DateTimeField(db_column='ENDTIME', blank=True, null=True)  # Field name made lowercase.
    closed = models.IntegerField(db_column='CLOSED', default=0)  # Field name made lowercase.
    expire_flag = models.IntegerField(db_column='EXPIRE_FLAG', default=0)  # Field name made lowercase.
    updated = models.DateTimeField(db_column='LAST_UPDATE', auto_now=True)  # Field name made lowercase.

    def get_absolute_url(self):
        return reverse('case-detail', kwargs={'pk': self.pk})

    class Meta:
        managed = True
        db_table = 'CASES'

class CaseEvents(models.Model):
    hready_id = models.ForeignKey(Cases, db_column='HREADY_ID', on_delete=models.CASCADE)  # Field name made lowercase.
    hadm_id = models.IntegerField(db_column='HADM_ID')  # Field name made lowercase.
    event_num = models.IntegerField(db_column='EVENT_NUM')  # Field name made lowercase.
    cgid = models.IntegerField(db_column='CGID')  # Field name made lowercase.
    description = models.CharField(db_column='DESCRIPTION', max_length=255)  # Field name made lowercase.

    class Meta:
        managed = True
        db_table = 'CASE_EVENTS'
