from django.shortcuts import redirect, render
from django.urls import path, include

from .views import ChartData, HomeView

app_name = 'charts'

urlpatterns = [
    path('', HomeView.as_view(), name='chart'),
    path('data/', ChartData.as_view(), name='chart-data')
]
