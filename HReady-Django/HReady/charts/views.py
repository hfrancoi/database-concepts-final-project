from django.shortcuts import render

from django.contrib.auth import get_user_model
from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import View

from rest_framework.views import APIView
from rest_framework.response import Response

from system.models import Subjects, HospitalGeneralInfo, Cases

from collections import defaultdict

import pickle

# Create your views here.

class HomeView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'charts/charts.html', {"customers": 10})


class ChartData(APIView):
    authentication_classes = []
    permission_classes = []
    
    def get(self, request, format=None):
        #case_counts = Cases.objects.group_by(endtime__year__in=case_years).count() # replace these with actual SQL by next wednesday
        case_counts = Cases.objects.raw('SELECT HREADY_ID, WEEK(STARTTIME) as week, count(*) as week_counts FROM CASES GROUP BY WEEK(STARTTIME)')
        week_cases = {}
        for case in case_counts:
            week_cases[case.week] = case.week_counts

        case_hours = Cases.objects.raw('SELECT HREADY_ID, HOUR(STARTTIME) as hour, count(*) as hour_counts FROM CASES GROUP BY HOUR(STARTTIME)')
        hours = {}
        for item in case_hours:
            hours[item.hour] = item.hour_counts

        data = {
            "wlabels": week_cases.keys(),
            "hlabels": hours.keys(),
            "wvalues": week_cases.values(),
            "hvalues": hours.values()
        }
        
        
        return Response(data) 
