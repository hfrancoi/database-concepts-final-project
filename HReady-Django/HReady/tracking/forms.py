from django import forms

HOSPITAL_TYPES=(
    ('Acute Care Hospitals', 'Acute Care Hospitals'),
    ('Critical Care Hospitals', 'Critical Care Hospitals'),
    ('Childrens','Childrens'),
)

class HospitalSearch(forms.Form):
    street_addy = forms.CharField(label='Street Address', max_length = 100)
    city = forms.CharField(label='City', max_length = 100)
    st_abrv = forms.CharField(label='State Abbrv.', max_length = 10)
    hospital_type = forms.CharField(label='Hospital Type', widget=forms.Select(choices=HOSPITAL_TYPES))
