from geopy import geocoders
from geopy.geocoders import MapBox

m =  MapBox(api_key="pk.eyJ1IjoidG9uaXRvbSIsImEiOiJjanA5YjJxczEwOG10M2tzNHZpd2I2bWVjIn0.w3fYGdxRk_BmXwVD_KvuEQ")

#Create empty list to store results

inputAddress= '375 18 Avenue, Paterson, NJ'


location = m.geocode(inputAddress)
print(location.latitude, location.longitude)
print(location.raw)
