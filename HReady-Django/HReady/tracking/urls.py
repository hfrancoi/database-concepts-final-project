from django.urls import path, include
from django.shortcuts import redirect, render
from . import views as tracking_views

app_name = 'tracking'

urlpatterns = [
    path('', tracking_views.TrackingView.as_view(), name = 'tracking'),
    path('ip', tracking_views.IpAddressView.as_view(), name = 'ip'),
    path('HospitalSearch', tracking_views.hospitalSearch, name='hsearch'),
    path('results', tracking_views.HospitalResultsView.as_view(), name='results'),
]
