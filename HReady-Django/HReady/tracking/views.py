from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.generic import CreateView, TemplateView, FormView, View
from .forms import HospitalSearch
from geopy import geocoders, distance
from geopy.geocoders import MapBox 
from mapbox import DirectionsMatrix 
from system.models import HospitalGeneralInfo
from rest_framework.response import Response
import json

m= MapBox(api_key="pk.eyJ1IjoidG9uaXRvbSIsImEiOiJjanA5YjJxczEwOG10M2tzNHZpd2I2bWVjIn0.w3fYGdxRk_BmXwVD_KvuEQ")

class TrackingView(TemplateView):
    template_name = 'tracking/gmaps.html'

class IpAddressView(TemplateView):
    template_name = 'tracking/base.html'


class HospitalSearchView(FormView):
    template_name = 'tracking/search.html'
    form_class = HospitalSearch


def hospitalSearch(request):
    form = HospitalSearch(request.POST or None)
    if form.is_valid():
        state_form = form.cleaned_data['st_abrv']
        hosp_type = form.cleaned_data['hospital_type']
        hospitals = HospitalGeneralInfo.objects.filter(state=state_form, hospital_type = hosp_type)
        address_string=[form.cleaned_data['street_addy'], form.cleaned_data['city'], form.cleaned_data['st_abrv']]
        address_string = ",".join(address_string) 
        location = m.geocode(address_string)
        origin_loc = (location.latitude, location.longitude)
        
        location_db = [m.geocode(hospital.address) for hospital in hospitals]
        destin_loc = [(location.latitude, location.longitude) for location in location_db]
        distances = [distance.distance(origin_loc, destination_location) for destination_location in destin_loc]
        distance_min = min(distances)
        index = distances.index(min(distances))
        target = location_db[index] 
    
        context = {
            'target': target,
            'distance': distance_min
        }
        return render(request, 'tracking/search.html', context)
        #return results(request, context, 'tracking/results.html') 

    else:
        form = HospitalSearch()
        return render(request, 'tracking/search.html', {'form':form})   


class HospitalResultsView(View):
    template_name= 'tracking/results.html'

def results(request, context, template):
    return render(request, template, context)


